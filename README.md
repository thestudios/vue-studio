<p align="center">
</p>

## Installation

```bash
npm install studio-ui
```

## Usage

```javascript
import Vue from 'vue';
import StudioUi from 'studio-ui';

Vue.use(StudioUi);
```
