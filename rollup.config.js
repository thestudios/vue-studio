import commonjs from '@rollup/plugin-commonjs';
import vue from 'rollup-plugin-vue';

export default {
  input: 'src/index.js',
  output: {
    name: 'StudioUi',
    exports: 'named',
    globals: {
      vue: 'Vue',
    },
  },
  plugins: [
    commonjs(),
    vue({
      css: true,
      compileTemplate: true,
    }),
  ],
  external: [
    'vue',
    'featherIcons',
  ],
};
