import XIcon from './components/XIcon.vue';
import XInput from './components/XInput.vue';
import XModal from './components/XModal.vue';
import XTable from './components/XTable.vue';

import XAlert from './plugins/Alert';
import XConfirm from './plugins/Confirm';

export function install (Vue) {
  if (install.installed) return;

  install.installed = true;

  Vue.component('x-icon', XIcon);
  Vue.component('x-input', XInput);
  Vue.component('x-modal', XModal);
  Vue.component('x-table', XTable);

  Vue.use(XAlert);
  Vue.use(XConfirm);
}

// create module definition for Vue.use()
const plugin = {
  install,
};

// auto install
let GlobalVue = null;
if (typeof window !== 'undefined') {
  GlobalVue = window.Vue;
} else if (typeof global !== 'undefined') {
  GlobalVue = global.Vue;
}

if (GlobalVue) {
  GlobalVue.use(plugin);
}

export default plugin;
