import { enableAutoDestroy, shallowMount } from '@vue/test-utils';
import Icon from '../src/components/Icon.vue';

describe('Icon', () => {
  enableAutoDestroy(afterEach);

  it('has default structure', async () => {
    const wrapper = shallowMount(Icon, {
      propsData: {
        name: 'user',
      },
    });

    expect(wrapper.element.tagName).toBe('svg');
    expect(wrapper.attributes('fill')).toBe('none');
    expect(wrapper.attributes('stroke')).toBe('currentColor');
  });
});
